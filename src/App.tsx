import React from 'react';
import Background from './Components/Background/Background';
import CalendarMain from './Components/Calendar/Calendar';
import styled from "styled-components";
import SideMenu from './Components/SideMenu/SideMenu';
import { ReusableProvider } from "reusable";
import Modal from './Components/Modal/Modal';
const Wrapper = styled.div`
  display: flex;
  margin: 0;
  padding: 0;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  font-family: "Nunito", sans-serif;
`;
const App = () => {
  
  return (
    <ReusableProvider>
      <Wrapper>
        <SideMenu />
        <Background />
        <CalendarMain />
        <Modal />
      </Wrapper>
    </ReusableProvider>
  );
}

export default App;
