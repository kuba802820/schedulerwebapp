import { useState } from "react"
import { createStore } from "reusable";
const useModalHook = () => {
    const [isOpen, setOpen] = useState(false);
    const [data, setData] = useState<[string, any][]>();
    return {
        data,
        setData,
        isOpen,
        setOpen
    }
}
export const useModal = createStore(useModalHook);