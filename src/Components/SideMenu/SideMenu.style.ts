import styled from "styled-components";

export const SideBar = styled.div`
top: 0;
left:0;
  width: 15vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: #512da8;
`;

export const Li = styled.li<{ isSelectedMonth: boolean }>`
  width: 100%;
  height: 50px;
  list-style-type: none;
  text-align: center;
  line-height: 50px;
  color: white;
  background-color: ${props => props.isSelectedMonth && "#673ab7"};
  transition: all .2s;
  &&:hover {
    cursor: pointer;
    background: #673ab7;
  }
`;
export const YearChanger = styled.div`
  display: flex;
  color: white;
  font-size: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
`;
export const IconYearChange = styled.img`
  width: 25px;
  height: 25px;
  &&:hover{
    cursor: pointer;

  }
`;