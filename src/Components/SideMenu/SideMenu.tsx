import React from "react";
import next from '../../Static/next.svg';
import back from "../../Static/back.svg";
import { useCalendar } from "../../Hooks/useCallendarData";
import { SideBar, YearChanger, IconYearChange, Li } from "./SideMenu.style";


const SideMenu = () => { 
  const startRange = 2020;
  const endRange = 2050
    const months = [
      "Styczeń",
      "Luty",
      "Marzec",
      "Kwiecień",
      "Maj",
      "Czerwiec",
      "Lipiec",
      "Sierpień",
      "Wrzesień",
      "Paźdzernik ",
      "Listopad",
      "Grudzień",
    ];

    const { month, setMonth, year, setYear } = useCalendar(); 
    const YearNext = () => setYear(prev => prev + 1);
    const YearPrev = () => setYear(prev => prev - 1);
    return (
      <SideBar>
        <YearChanger>
          {year > startRange && (
            <IconYearChange src={back} alt="Back" onClick={YearPrev} />
          )}
          {year}
          {year < endRange && (
            <IconYearChange src={next} alt="Next" onClick={YearNext} />
          )}
        </YearChanger>
        {months.map((v, i) => {
          return (
            <Li key={v}
              onClick={() => {
                setMonth(i);
              }}
              isSelectedMonth={month === i ? true : false}
            >
              {v}
            </Li>
          );
        })}
      </SideBar>
    );

}

export default SideMenu;