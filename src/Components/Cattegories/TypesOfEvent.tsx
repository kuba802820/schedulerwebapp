import React from "react";
import { Label } from "./TypesOfEvent.style";
import { useModal } from "../../Hooks/useModal";

export interface IData { 
  id: number;
  type: string; 
  color: string;
  name: string;
  day: number;
  year: number;
  month: number;
  schoolsubject: string;
  content: string;
}

type MyGroupType = {
    [key:string]: IData;
}

interface IProps {
  data: IData[], 
  selectedDay: number,
  calledarData: {
    month: number,
    year: number,
  },
}

const TypesOfEvent = ({ data, selectedDay, calledarData }: IProps) => {
  const {setOpen, setData} = useModal();
  return (
    <>
      {data.map(
        (v, i) =>
          selectedDay === v.day &&
          calledarData.month === v.month &&
          calledarData.year === v.year && (
            <Label
              color={v.color}
              key={v.id}
              onClick={() => {
                setOpen(true);
                setData(Object.entries(v));
              }}
            >
              {v.name}
            </Label>
          )
      )}
    </>
  );
};

export default TypesOfEvent;
