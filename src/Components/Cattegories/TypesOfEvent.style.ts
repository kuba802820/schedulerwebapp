import styled from "styled-components";
export const Label = styled.div<{ color: string }>`
  width: 100%;
  height: 25px;
  margin-top: 10px;
  color: #fff;
  border-radius: 10px;
  padding: 3px;
  line-height: 25px;
  font-size: 15px;
  background-color: ${props => props.color};
  transition: all .2s;
  &&:hover{
    cursor: pointer;
    filter: brightness(1.2);
  }
`;