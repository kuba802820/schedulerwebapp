import styled from 'styled-components'

export const Td = styled.td<{ isEnabled: boolean }>`
  width: 100vw;
  height: auto;
  background-color: #${props => (props.isEnabled ? "fff" : "transparent")};
  margin: 5px;
  text-align: center;
  padding: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  border-radius: 10px;
  font-size: 20px;
  overflow-y: auto;
`;