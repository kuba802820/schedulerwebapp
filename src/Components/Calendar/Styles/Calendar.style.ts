import styled from "styled-components";

export const Table = styled.table`
  width: 100%;
  height: 80%;
  margin-left: 20px;
  margin-right: 20px;
  background: #673ab7;
  border-radius: 10px;
  overflow-y: auto;
  padding: 20px 10px 0px 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;

export const Tbody = styled.tbody`
 
`;

export const Tr = styled.tr`
  width: 100%;
  margin-top: 20px;
  display: flex;
  justify-content: space-around;
`;
export const Events = styled.div`
  width: 100%;
  height: 100%;
`;
export const Th = styled.th`
    width: 100%;
    background: #fff;
    padding: 20px;
    &&:first-child{
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }
    &&:last-child{
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
    }
`;