import React from 'react';
import { Calendar } from 'calendar';
import Item from "./Item";
import TypesOfEvent from "../Cattegories/TypesOfEvent";
import { useCalendar } from "../../Hooks/useCallendarData";
import { Table, Tr, Th } from "./Styles/Calendar.style";

const CalendarMain = () => {
    const example = [
      {
        id: 449,
        type: "Test",
        color: "#aa0000",
        name: "Sprawdzian",
        day: 5,
        year: 2020,
        month: 1,
        schoolsubject: "j.polski",
        content: "asad"
      },
      {
        id: 447,
        type: "Test",
        color: "#4caf50",
        name: "Kartkówka",
        day: 1,
        year: 2021,
        month: 5,
        schoolsubject: "matematyka",
        content: "asad"
      },
      {
        id: 24,
        type: "Test",
        color: "#42a5f5",
        name: "Praca Domowa",
        day: 10,
        month: 6,
        year: 2022,
        schoolsubject: "matematyka",
        content: "asad"
      },
      {
        id: 11,
        type: "Test",
        color: "#ff920d",
        name: "Coś innego",
        day: 10,
        year: 2020,
        month: 0,
        schoolsubject: "matematyka",
        content: "asad"
      },
      {
        id: 4411,
        type: "Test",
        color: "#ff920d",
        name: "Coś innego",
        day: 9,
        year: 2020,
        month: 0,
        schoolsubject: "matematyka",
        content: "asad"
      },
      {
        id: 4222,
        type: "Test",
        color: "#ff920d",
        name: "Coś innego",
        day: 10,
        year: 2020,
        month: 0,
        schoolsubject: "matematyka",
        content: "asad"
      },
      {
        id: 432,
        type: "Test",
        color: "#ff920d",
        name: "Coś innego",
        day: 10,
        year: 2020,
        month: 0,
        schoolsubject: "matematyka",
        content: "asad"
      },
      {
        id: 44,
        type: "Test",
        color: "#ff920d",
        name: "Coś innego",
        day: 10,
        year: 2020,
        month: 0,
        schoolsubject: "matematyka",
        content: "asad"
      }
    ];
    const cal = new Calendar(1);
    const callendarStore = useCalendar();
    const month: any = cal.monthDays(callendarStore.year, callendarStore.month);
    return (
      <>
        <Table>
          <thead>
            <Tr>
              <Th>Poniedziałek</Th>
              <Th>Wtorek</Th>
              <Th>Środa</Th>
              <Th>Czwartek</Th>
              <Th>Piątek</Th>
              <Th>Sobota</Th>
              <Th>Niedziela</Th>
            </Tr>
          </thead>
          <tbody>
            {month.map((v: any[], i: any) => {
              return (
                <Tr key={i}>
                  {v.map((k, i) => {
                    return (
                      <Item
                        key={i}
                        day={k !== 0 ? k : ""}
                        isEnabled={k !== 0 ? true : false}
                      >
                        {k !== 0 ? (
                          <TypesOfEvent
                            data={example}
                            selectedDay={k}
                            calledarData={callendarStore}
                          />
                        ) : (
                          <div></div>
                        )}
                      </Item>
                    );
                  })}
                </Tr>
              );
            })}
          </tbody>
        </Table>
      </>
    );
}

export default CalendarMain;