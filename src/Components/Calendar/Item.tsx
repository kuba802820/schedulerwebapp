import React from 'react';
import { Td } from './Styles/Item.style';

const Item = ({
  children,
  day,
  isEnabled
}: {
  children?: JSX.Element | false;
  day: any;
  isEnabled: boolean;}) => {
  return (
    <>
      <Td isEnabled={isEnabled}>
        <div>{day}</div>
        {children}
      </Td>
    </>
  );
};

export default Item;