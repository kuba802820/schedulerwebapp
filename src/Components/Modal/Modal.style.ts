import styled from "styled-components";
export const Overlay = styled.div`
position: absolute;
top: 0;
left: 0;
opacity: 0.6;
width: 100%;
height: 100%;
background: #000;

`;
export const ModalWrapper = styled.div`
    position: absolute;
    transform: translate(-50%,-50%);
    top: 50%;
    left: 50%;
    border-radius: 10px;
    width: 500px;
    height: 500px;
    background-color: #fff;
    display: flex;
    overflow: hidden;
    align-items: flex-end;
    justify-content:center;
    flex-direction: column;
`;
export const DataItem = styled.div`
    width: 100%;
    margin-top: 2px;
    height: 6vh;
    text-align: center;
    line-height: 6vh;
    color: white;
    background-color: #4527A0;
`
export const DataContent = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content:center;
    height: 90%;
`;
export const CloseBtn = styled.div`
    width: 100%;
    height: 60px;
    color: white;
    text-align: center;
    line-height: 60px;
    background-color: #673AB7;
    transition: all .2s;
    &&:hover{
        cursor: pointer;
        background-color: #7E57C2;
    }
`;
