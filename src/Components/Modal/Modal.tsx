import { ModalWrapper, Overlay, CloseBtn, DataContent, DataItem } from "./Modal.style";
import React, { useState, useEffect, useCallback } from "react";
import { useModal } from "../../Hooks/useModal";

const Modal = () => {
    const { isOpen, setOpen, data } = useModal();
    const [filterData, setFilterData] = useState<[string, any][]>([]);
    const ignoreKeys = ["color","day","month","year"];
    const setFilterDataMemo = useCallback((arr: [string, any][])=>{
      setFilterData(arr);
    },[])
    useEffect(() => {
     if (data !== undefined) {
       const arr = data.filter(x => {
         if (!ignoreKeys.includes(x[0])) return x;
       });
       setFilterDataMemo(arr);
     }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data])
    return (
      <>
        {isOpen && (
          <>
            <Overlay></Overlay>
            <ModalWrapper>
              <DataContent>
                {data !== undefined && (
                  <div>
                      {
                        filterData.map((v,i)=> <DataItem>{v[0]}: {v[1]}</DataItem>)
                      }
                  </div>
                )}
              </DataContent>
              <CloseBtn onClick={() => setOpen(false)}>Zamknij</CloseBtn>
            </ModalWrapper>
          </>
        )}
      </>
    );
}
export default Modal;